from setuptools import setup

setup(
    name='exkeylibur',
    version='0.0.1',
    description='Automatically extract keyword from R&D text combining Tf-idf and TextRank',
    url='https://nduransiris@bitbucket.org/nduransiris/exkeylibur.git',
    author='Nicolau Duran & Francesco Massucci, SIRIS Lab',
    author_email='nicolau.duransilva@sirisacademic.com',
    license='GPLv3',
    packages=['exkeylibur'],
    zip_safe=False
)
